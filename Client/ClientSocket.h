#pragma once
#define WIN32_LEAN_AND_MEAN

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#define _CRT_SECURE_NO_WARNINGS
#include "winsock2.h"
#include <iostream>

class Socket
{
public:
	Socket()
	{
		// Create socket
		clientSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);

		//error checking																																		
		if (clientSocket == INVALID_SOCKET)
		{
			int errorcode = WSAGetLastError();
			std::cout << "Error on creating socket: " << errorcode << std::endl;
			WSACleanup();
		}
	}
	int BindSocket(const sockaddr * name, int size);
	int ConnectSocket(const sockaddr * remote, int size);
	int SendSocket(const char * buffer, int buffer_len, int flags);
	int ReceiveSocket(char * buffer, int buffer_len, int flags);
	void ShutdownSocket();
	void CloseSocket();
	SOCKET clientSocket;
	int ret;
};