//get rid of all the crap in windows.h that we won't ever use
#define WIN32_LEAN_AND_MEAN
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#include "windows.h"
#include "ClientSocket.h"
//usual stuff
#include <iostream>
using std::cout; using std::cin; using std::endl;
#include <string>
using std::string;
// fstream::open / fstream::close
#include <fstream>      // std::fstream
//winsock
#include "winsock2.h"

//need for getnameinfo() function
#include "Ws2tcpip.h"

//link the library
#pragma comment(lib, "ws2_32.lib")

//length of our buffers
const int BUF_LEN = 255;

//our remote port that we shall connect to
const int remote_port = 3000u;


int main()
{

	string port;
	string IP;
	std::fstream fs;
	fs.open("ClientConfig.txt", std::fstream::in);

	if (fs.fail() == true) {
		return -1;
	}
	std::getline(fs, port);
	cout <<"Port: " <<port << endl;
	std::getline(fs, IP);
	cout << "IP: " << IP << endl;

	fs.close();
	//our buffers
	char receiveBuffer[BUF_LEN];
	char sendBuffer[BUF_LEN];

	char* password = "notmypassword";
	SecureZeroMemory(sendBuffer, BUF_LEN);
	SecureZeroMemory(receiveBuffer, BUF_LEN);


	int ret = 0;
	int rcounter = 0;

	WSADATA wsaData;
	SecureZeroMemory(&wsaData, sizeof(wsaData));

	// Initialize the library
	ret = WSAStartup(MAKEWORD(2, 2), &wsaData);
	std::cout << "CLIENT\n";
	//check that WSAStartup was sucessful.  
	if (ret != 0) {
		cout << "Error initializing winsock!  Error was: " << ret << endl;
		return ret;
	}
	//std::cout << "Library initialized\n";


	struct sockaddr_in socketAddress;
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_port = 0;
	
	hostent* localhost;
	localhost = gethostbyname("");
	char* localIP;
	localIP = inet_ntoa(*(in_addr*)*localhost->h_addr_list);

	//let's use the address that we got from gethostbyname for our local endpoint
	socketAddress.sin_addr.s_addr = inet_addr(localIP);

	sockaddr_in remote;
	remote.sin_family = AF_INET;
	remote.sin_addr.S_un.S_addr= inet_addr(localIP);
	remote.sin_port = htons(3000);

	Socket clientSocket;
																																	

	// Bind the socket
	ret = clientSocket.BindSocket((sockaddr*)&socketAddress, sizeof(socketAddress));
	
	// Connect the socket
	ret = clientSocket.ConnectSocket((sockaddr*)&remote, sizeof(remote));
	
	do
	{
		string exitcommand = "exit";
		std::cout << "Type a message: ";
		cin.getline(sendBuffer, BUF_LEN);
		if (sendBuffer == exitcommand)
		{
			break; 
		}

		ret = clientSocket.SendSocket(sendBuffer, 100, 0);
		SecureZeroMemory(sendBuffer, BUF_LEN);

		rcounter = clientSocket.ReceiveSocket(receiveBuffer, BUF_LEN, 0);
		if (rcounter == SOCKET_ERROR)
		{
			rcounter = WSAGetLastError();
			break;
		}
		
		if (rcounter == 0)
			break;
		string s;
		s.assign(receiveBuffer, rcounter);
		
		std::cout << "Message from Forward: " << s << std::endl;
		SecureZeroMemory(receiveBuffer, BUF_LEN);


	} while (true);

	clientSocket.ShutdownSocket();
	clientSocket.CloseSocket();
	WSACleanup();

	cout << "done" << endl;


}