#define WIN32_LEAN_AND_MEAN

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
using std::cout; using std::cin; using std::endl;
#include <string>
// fstream::open / fstream::close
#include <fstream>      // std::fstream
using std::string;
#include "winsock2.h"
#pragma comment(lib, "ws2_32.lib")
#include "MySocket.h"
//buffer length, this is arbitrary, but note that I use a const and not a #define
const int BUF_LEN = 255;

int main()
{
	string port;
	std::fstream fs;
	fs.open("ServerConfig.txt", std::fstream::in);

	if (fs.fail() == true) {
		return -1;
	}

	std::getline(fs, port); //3rd character to limit 
	cout << "Port: " << port << endl;
	fs.close();
	int result = 0;

	//WSAData, same as in the client
	WSADATA wsData;

	//host info
	char* localIP;
	hostent* localhost;

	//our receive buffer
	char receiveBuffer[BUF_LEN];
	SecureZeroMemory(receiveBuffer, BUF_LEN);

	//initialize winsock
	result = WSAStartup(MAKEWORD(2, 2), &wsData);
	std::cout << "SERVER\n";
	//std::cout << "Library initialized\n";
	if (result) {
		printf("Error starting winsock: %d", result);
		return result;
	}

	//our listener socket
	Socket listenerSocket;
	struct sockaddr_in socketAddress;

	//get local host IP, same as in client demo
	localhost = gethostbyname("");
	localIP = inet_ntoa(*(in_addr*)*localhost->h_addr_list);

	
	//create local endpoint for listening.
	socketAddress.sin_family = AF_INET;
	//this time, set the port to something that's greater than 1024
	socketAddress.sin_port = htons(8080);
	socketAddress.sin_addr.s_addr = inet_addr(localIP);


	//std::cout << "Listener socket created\n";


	//bind our listener socket to local endpoint
	result = listenerSocket.BindSocket((SOCKADDR*)&socketAddress, sizeof(socketAddress));

	//std::cout << "Listener socket bound\n";
	//now we listen for someone to connect.  This will put the socket into a listening
	//state, but will not automatically accept a connection
	result = listenerSocket.ListenSocket(10);

	
	// Create client socket
	Socket clientSocket;
	sockaddr_in remoteEndpoint;
	SecureZeroMemory(&remoteEndpoint, sizeof(remoteEndpoint));

	int endpointSize = sizeof(remoteEndpoint);

	// accept
	clientSocket = listenerSocket.acceptSocket((sockaddr*)&remoteEndpoint, &endpointSize);

	int rcounter = 0;
	//we're going to keep listening for data until the client closes the 
	//connection. 
	do
	{
		rcounter = clientSocket.ReceiveSocket(receiveBuffer, BUF_LEN, 0);
		if (rcounter == 0 || rcounter == SOCKET_ERROR)
		{
			clientSocket = listenerSocket.acceptSocket((sockaddr*)&remoteEndpoint, &endpointSize);
		
		}
		else 
		{
			string s;
			s.assign(receiveBuffer, rcounter);
			std::cout << "Message from the Forward: " << s << "\n";

			std::cout << "Message for the Client: " << receiveBuffer << std::endl;
			clientSocket.SendSocket(receiveBuffer, 100, 0);
			SecureZeroMemory(receiveBuffer, BUF_LEN);
		}
		
		
	} while (true);


	printf("Shutdown received, ending loop");

	//shut down our sockets, both the one we originally created
	//and the one we got when the connection was established
	clientSocket.ShutdownSocket();
	listenerSocket.ShutdownSocket();

	//close the sockets
	clientSocket.CloseSocket();
	listenerSocket.CloseSocket();


	WSACleanup();

	cin.get();
}