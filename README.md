cs260_monica.marquez_assignment1

In the client window type a message different than "exit", which is the command to close the client window. 
If you close the client window, the server will wait to another client so you can open a new one and the 
connection will be established again.