#include  "ClientSocket.h"
int Socket::BindSocket(const sockaddr * name, int size)
{
	// Bind the socket
	ret = bind(clientSocket, name, size);

	if (ret == SOCKET_ERROR)
	{

		ret = WSAGetLastError();
		return ret;
	}

}

int Socket::ConnectSocket(const sockaddr * remote, int size)
{
	// Connect the socket
	ret = connect(clientSocket, remote, size);
	if (ret == SOCKET_ERROR) {
		ret = WSAGetLastError();
		return ret;
	}

	//std::cout << "Connection established\n";

}

int Socket::SendSocket(const char * buffer, int buffer_len, int flags)
{
	ret = send(clientSocket, buffer, buffer_len, flags);
	//SecureZeroMemory(buffer, buffer_len);
	return ret;
}

int Socket::ReceiveSocket(char * buffer, int buffer_len, int flags)
{
	int rcounter = 0;
	rcounter = recv(clientSocket, buffer, buffer_len, flags);
	return rcounter;
}

void Socket::ShutdownSocket()
{
	shutdown(clientSocket, SD_BOTH);
}

void Socket::CloseSocket()
{
	closesocket(clientSocket);
}