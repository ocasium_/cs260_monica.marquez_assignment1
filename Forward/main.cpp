#define WIN32_LEAN_AND_MEAN

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#include <iostream>
using std::cout; using std::cin; using std::endl;
#include <string>
// fstream::open / fstream::close
#include <fstream>      // std::fstream
using std::string;
#include "winsock2.h"
#pragma comment(lib, "ws2_32.lib")
#include "MySocket.h"
//buffer length, this is arbitrary, but note that I use a const and not a #define
const int BUF_LEN = 255;

int main()
{
	string port_client;
	string port_server;
	string IP_address;
	std::fstream fs;
	fs.open("ForwardConfig.txt", std::fstream::in);

	if (fs.fail() == true) {
		return -1;
	}

	std::getline(fs, port_client); //3rd character to limit 
	cout << "Port Client: " << port_client << endl;
	std::getline(fs, port_server);
	cout << "Port Server: " << port_server << endl;
	std::getline(fs, IP_address);
	cout << "IP: " << IP_address << endl;

	fs.close();
	int result = 0;
	int res_send = 0;
	//WSAData, same as in the client
	WSADATA wsData;

	//host info
	char* localIP;
	hostent* localhost;

	//our receive buffer
	char receiveBuffer[BUF_LEN];
	SecureZeroMemory(receiveBuffer, BUF_LEN);

	//initialize winsock
	result = WSAStartup(MAKEWORD(2, 2), &wsData);
	std::cout << "FORWARD\n";
	//std::cout << "Library initialized\n";
	if (result) {
		printf("Error starting winsock: %d", result);
		return result;
	}

	//our listener socket
	Socket listenerSocket;
	Socket senderSocket;
	struct sockaddr_in socketAddress;
	struct sockaddr_in senderSocket_address;

	//get local host IP, same as in client demo
	localhost = gethostbyname("");
	localIP = inet_ntoa(*(in_addr*)*localhost->h_addr_list);


	//create local endpoint for listening.
	socketAddress.sin_family = AF_INET;
	socketAddress.sin_port = htons(3000); 	//this time, set the port to something that's greater than 1024
	socketAddress.sin_addr.s_addr = inet_addr(localIP);

	//create local endpoint for sending

	senderSocket_address.sin_family = AF_INET;
	senderSocket_address.sin_port =0; 	//this time, set the port to something that's greater than 1024
	senderSocket_address.sin_addr.s_addr = inet_addr(localIP);

	//bind our listener socket to local endpoint
	result = listenerSocket.BindSocket((SOCKADDR*)&socketAddress, sizeof(socketAddress));

	//bind our sender socket to loca endpoint
	res_send = senderSocket.BindSocket((SOCKADDR*)&senderSocket_address, sizeof(senderSocket_address));

	//std::cout << "Listener socket bound\n";
	//std::cout << "Sender socket bound\n";

	result = listenerSocket.ListenSocket(10);


	// Create client socket
	Socket clientSocket;
	sockaddr_in remoteEndpoint;
	sockaddr_in remote;
	remote.sin_family = AF_INET;
	remote.sin_addr.S_un.S_addr = inet_addr(localIP);
	remote.sin_port = htons(8080);
	SecureZeroMemory(&remoteEndpoint, sizeof(remoteEndpoint));

	int endpointSize = sizeof(remoteEndpoint);
	int remoteSize = sizeof(remote);
	// accept
	clientSocket = listenerSocket.acceptSocket((sockaddr*)&remoteEndpoint, &endpointSize);
	
	res_send = senderSocket.ConnectSocket((sockaddr*)&remote, remoteSize);


	int rcounter = 0;
	int rcounter_ = 0;
	//we're going to keep listening for data until the client closes the 
	//connection. 
	do
	{
		rcounter = clientSocket.ReceiveSocket(receiveBuffer, BUF_LEN, 0);
		if (rcounter == 0 || rcounter == SOCKET_ERROR)
		{
			clientSocket = listenerSocket.acceptSocket((sockaddr*)&remoteEndpoint, &endpointSize);

		}
		else
		{
			string s;
			s.assign(receiveBuffer, rcounter);
			
			std::cout << "Message from the Client: " << s << "\n";

			if (senderSocket.SendSocket(receiveBuffer, 100, 0) == SOCKET_ERROR)
				std::cout << "WE HAvE A PROBLEM\n";
			std::cout << "Message for the Server: " << s << "\n";
			SecureZeroMemory(receiveBuffer, BUF_LEN);
			senderSocket.ReceiveSocket(receiveBuffer, 100, 0);
			string s_;
			s_.assign(receiveBuffer, rcounter);
			std::cout << "Message from the Server: " << s_ << "\n";

			clientSocket.SendSocket(receiveBuffer, 100, 0);
			std::cout << "Message for the Client: " << s_ << "\n";
		}


	} while (true);

	printf("Shutdown received, ending loop");


	senderSocket.ShutdownSocket();
	clientSocket.ShutdownSocket();
	listenerSocket.ShutdownSocket();

	//close the sockets
	senderSocket.CloseSocket();
	clientSocket.CloseSocket();
	listenerSocket.CloseSocket();


	WSACleanup();
	cin.get();
}